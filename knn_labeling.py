import pandas as pd
import numpy as np
import glob
import pickle
import matplotlib.pyplot as plt
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import LogisticRegressionCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
db = 'public_notices'
pswd = ''
usr = ''
import sqlalchemy
from sqlalchemy import create_engine
import json


engine = create_engine('mysql+pymysql://' + usr + ':' + pswd + '@127.0.0.1/' + db)
conn = engine.connect() 

sel = conn.execute("select * from unlabeled_notices")

unlabeled_df = pd.DataFrame(sel.fetchall())
unlabeled_df.columns = sel.keys()

n = 5
#model = pickle.load(open('./models/knn_w2v_' + str(n), 'rb'))
model = pickle.load(open('./models/final/large_svm_w2v' , 'rb'))


for idx, row in unlabeled_df.iterrows():
    data = np.reshape(np.array(json.loads(row['word_vec'])), (1,-1))
    
    if np.isnan(data).all():
        continue
 
    label = model.predict(data)
    unlabeled_df.at[idx, 'notice_type'] = float(label[0])


print('Saving to DB...')

unlabeled_df.to_sql('svm_label', conn, index=False, if_exists='append', chunksize=5000)

