import pandas as pd
import numpy as np
import glob
import matplotlib.pyplot as plt
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import accuracy_score
from sklearn.model_selection import cross_val_score

path =""# use your path
allFiles = glob.glob(path + "/*.tsv")
frame = pd.DataFrame()
list_ = []
#print(allFiles)
for file_ in allFiles:
    df = pd.read_csv(file_, sep='\t')
    #print(df.columns)
    list_.append(df)

frame = pd.concat(list_)
#frame = pd.read_csv('curr_train.csv')

pd.set_option('display.max_colwidth', -1)
frame = pd.read_csv('curr_train.csv')
frame.drop('Unnamed: 0',axis=1, inplace=True)
frame.apply(lambda x: x.str.strip() if x.dtype == 'object' else x)

#frame.to_csv("matt_label_grouped.csv")
def fix_labels(label):
    if label == None:
        return 'Misc'
    if label == 'family/personal' or label =='Family/personal':
        return 'Personal/family'
    
    return label.capitalize()

frame['notice_type'] = frame['notice_type'].apply(fix_labels)

def labelEncoder(new_df,cat_cols):
    from sklearn import preprocessing
    for col in cat_cols:
        lbl = preprocessing.LabelEncoder()
        lbl.fit(list(new_df[col].values.astype('str')))
        new_df[col+"_numeric"] = lbl.transform(list(new_df[col].values.astype('str')))
    return new_df

def add_len_msg_ft(new_df,column_String):
    new_df['msg_len'] = frame[column_String].str.len()
    return new_df


###########################
#MSG LEN BASELINE
###########################

from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import LogisticRegressionCV
new_df = frame.copy()
new_df = labelEncoder(new_df,["notice_type"])
new_df = add_len_msg_ft(new_df,"content")

new_df['msg_len'] = (new_df['msg_len'] - new_df['msg_len'].mean()) #/ new_df['msg_len'].std()

#print(new_df['msg_len'][:10])
#print(new_df['notice_type_numeric'].value_counts())

from sklearn.model_selection import train_test_split
X = new_df["msg_len"]
y = new_df["notice_type_numeric"]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

X_train = X_train.values.reshape(len(X_train),1)
X_test = X_test.values.reshape(len(X_test),1)
y_train = y_train.values.reshape(len(y_train))
y_test = y_test.values.reshape(len(y_test))

num_classes = y.nunique()


##LR
clf = LogisticRegressionCV(cv=5, random_state=0, solver='liblinear',multi_class='ovr', n_jobs=-1).fit(X_train, y_train)
ypred = clf.predict(X_test)
score = clf.score(X_test,y_test)
accuracy = accuracy_score(y_test, ypred)
precision, recall, fscore, support = precision_recall_fscore_support(y_test, ypred, average=None)

#print(clf.coef_)
#print(y_test)
#print(ypred)


print('accuracy: {}'.format(accuracy))
print('precision: {}'.format(precision))
print('recall: {}'.format(recall))
print('fscore: {}'.format(fscore))
print('support: {}'.format(support))


print('--------------------------------\n')
###SVM
from sklearn import svm
clf = svm.SVC(gamma=.2, decision_function_shape='ovr', C=20)
clf.fit(X_train,y_train)
svm_preds = clf.predict(X_test)
svm_score = clf.score(X_test, y_test)
y_score = clf.decision_function(X_test)
cross_val = cross_val_score(clf, X_train, y_train, cv=5)

#print(svm_preds)
#print(svm_score)
#print(y_score)
#print(clf.predict_proba(X_test))

accuracy = accuracy_score(y_test, svm_preds)
precision, recall, fscore, support = precision_recall_fscore_support(y_test, svm_preds, average=None)

#print(y_test)
#print(svm_preds)

print('Cross val accuracy: {}'.format(cross_val.mean()))
print('accuracy: {}'.format(accuracy))
print('precision: {}'.format(precision))
print('recall: {}'.format(recall))
print('fscore: {}'.format(fscore))
print('support: {}'.format(support))
print('--------------------------------\n')
#fig = plt.figure()
#plt.plot(recall, precision, marker='.')
#plt.xlabel('recall')
#plt.ylabel('precision')
#plt.show()

######
# Random guessing baseline
######

guesses = [np.random.randint(np.min(y_test), np.max(y_test) + 1) for i in range(len(y_test))]
guess_score = accuracy_score(y_test, guesses)
g_prec, g_recall, g_fscore, g_support = precision_recall_fscore_support(y_test, guesses, average=None)

print('accuracy: {}'.format(guess_score))
print('precision: {}'.format(g_prec))
print('recall: {}'.format(g_recall))
print('fscore: {}'.format(g_fscore))
print('support: {}'.format(g_support))
print('--------------------------------\n')

######
# Guess most common class
######

common_class = np.argmax(np.bincount(y_train))
common_guess = [common_class for i in range(len(y_test))]

common_score = accuracy_score(y_test, common_guess)
c_prec, c_recall, c_fscore, c_support = precision_recall_fscore_support(y_test, common_guess, average=None)


print('accuracy: {}'.format(common_score))
print('precision: {}'.format(c_prec))
print('recall: {}'.format(c_recall))
print('fscore: {}'.format(c_fscore))
print('support: {}'.format(c_support))
print('--------------------------------')
#

