import pandas as pd
import numpy as np
import glob
import pickle
import matplotlib.pyplot as plt
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import LogisticRegressionCV
from sklearn.model_selection import train_test_split
from sklearn import preprocessing



def labelEncoder(new_df, cat_cols):
    for col in cat_cols:
        lbl = preprocessing.LabelEncoder()
        lbl.fit(list(new_df[col].values.astype('str')))
        new_df[col + "_numeric"] = lbl.transform(list(new_df[col].values.astype('str')))

    return new_df


def fix_labels(label):
    if label == None:
        return 'Misc'
    if label == 'family/personal' or label == 'Family/personal':
        return 'Personal/family'

    return label.capitalize()


notice_df = pd.read_csv('./curr_train.csv')
notice_df.drop('Unnamed: 0', axis=1, inplace=True)
notice_df['notice_type'] = notice_df['notice_type'].apply(fix_labels)
notice_df = labelEncoder(notice_df, ['notice_type'])

# drop non-numeric type field
notice_df.drop('notice_type', axis=1, inplace=True)

# load in word vectors and group with appropriate level
train_vectors = []
for idx, row in notice_df.iterrows():
    doc_vec = load_obj('./data/doc2vec/notice_' + str(idx))
    label = np.array([row['notice_type_numeric']])

    if np.isnan(doc_vec).all() or np.isnan(label).any():
        print(idx)
        print(doc_vec)
        print(label)
        continue

    record = np.append(doc_vec, label)
    train_vectors.append(record)

# Convert to appropriate train/test dataframes
train_df = pd.DataFrame(train_vectors)
labels = train_df[50]  # get label data
train_df.drop(train_df.columns[len(train_df.columns) - 1], axis=1, inplace=True)  # drop label from input

train_data, test_data, train_labels, test_labels = train_test_split(train_df, labels, random_state=42)
num_classes = labels.nunique()

print("LR START")
##LR
model = LogisticRegressionCV(cv=5, random_state=0, solver='liblinear', multi_class='ovr').fit(train_data,train_labels)
print("MODEL_DONE")
ypred = model.predict(test_data)
accuracy = accuracy_score(test_labels, ypred)
precision, recall, fscore, support = precision_recall_fscore_support(test_labels, ypred, average=None)

print('accuracy: {}'.format(accuracy))
print('precision: {}'.format(precision))
print('recall: {}'.format(recall))
print('fscore: {}'.format(fscore))
print('support: {}'.format(support))
