import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import argparse
from pathlib import Path
import sys

df = pd.read_csv('annotated_data_0_20.tsv', sep='\t', dtype={'notice_id': 'int64', 'content': 'str'})
pd.set_option('display.max_colwidth', -1)
df.drop('Unnamed: 0',axis=1, inplace=True)
df.apply(lambda x: x.str.strip() if x.dtype == 'object' else x)


print(df.columns)
