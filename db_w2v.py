import gensim
from gensim.test.utils import common_texts
import numpy as np
import pandas as pd
import pickle
db = 'public_notices'
pswd = ''
usr = ''
import sqlalchemy
from sqlalchemy import create_engine
import json
import unicodedata


engine = create_engine('mysql+pymysql://' + usr + ':' + pswd + '@127.0.0.1/' + db)
conn = engine.connect() 
#sel = conn.execute("select * from hand_labeled_notices")
sel = conn.execute("select * from all_years_notices_nodup")

notice_df = pd.DataFrame(sel.fetchall())
notice_df.columns = sel.keys()

print('Loading data...')
public_notice_tokenized = dict()
for idx,row in notice_df.iterrows():
    k = row['notice_id']
    content = row['content']
    ascii_content = unicodedata.normalize('NFKD', content).encode('ascii', 'ignore')
    tokens = gensim.utils.simple_preprocess(ascii_content)
    public_notice_tokenized[k] = tokens

tokenized_notice_vocab = public_notice_tokenized.values()
model = gensim.models.Word2Vec(tokenized_notice_vocab,size=50, min_count=1, workers=-1)

print('Training w2v...')
embed_dict = dict()
print(model.iter)
for k,v in public_notice_tokenized.items():
    model.train([v], total_examples=model.corpus_count, epochs=50)
    
    all_notice_vectors = []
    
    for word in v:
        all_notice_vectors.append(model.wv[word])
    
    w2v_matrix = np.matrix(all_notice_vectors)
    w2v_matrix = np.mean(w2v_matrix,axis=0)
    word_vec = w2v_matrix.A1

    embedding = json.dumps(word_vec.tolist())
    embed_dict[k] = embedding 

print('Prepping output to DB...')
print(len(embed_dict.keys()))
final_data = []
cols = ['notice_id', 'word_vec']
for k,v in embed_dict.items():
    if k == 23630222:
        print(v)
    final_data.append([k,v]) # sort by week and then add user_id to each record

model.save('')

print('Saving to DB...')
print(len(final_data))

table = pd.DataFrame(final_data, columns=cols)
#table.head(2)
#conn.execute('drop table if exists all_notice_word_vectors')
table.to_sql('word_vectors_50epoch', conn, index=False, if_exists='append', chunksize=5000)
#conn.execute('alter table all_notice_word_vectors order by notice_id asc')

