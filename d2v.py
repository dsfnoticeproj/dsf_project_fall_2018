import gensim
from gensim.test.utils import common_texts
from gensim.models.doc2vec import Doc2Vec, TaggedDocument
import numpy as np
import pandas as pd
import pickle

#data_files = ['matt_label_grouped_0_100.csv']

notice_df = pd.read_csv('curr_train.csv')

def save_obj(obj, name):
    with open(name + '.pkl', 'wb+') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name):
    with open(name + '.pkl', 'rb') as f:
        return pickle.load(f)

print(len(notice_df['content']))
tokenized_notice_vocab = [TaggedDocument(doc, [i]) for i, doc in enumerate(notice_df['content'])]
model = Doc2Vec(tokenized_notice_vocab,vector_size=50, min_count=1, workers=12)

print(len(model.docvecs))
print(model.docvecs[0])
for i in range(len(model.docvecs)):
    model_name = 'notice_' + str(i)
    save_obj(model.docvecs[i], './data/doc2vec/' + model_name)
