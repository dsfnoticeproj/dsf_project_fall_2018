import pandas as pd
import numpy as np
db = 'public_notices'
pswd = ''
usr = ''
import sqlalchemy
from sqlalchemy import create_engine
import sys

notice_df = pd.read_csv('')
print(notice_df.columns)
#sys.exit(0)
notice_df.drop('Unnamed: 0',axis=1, inplace=True)
notice_df.drop('Unnamed: 0.1',axis=1, inplace=True)
notice_df.drop('Unnamed: 0.1.1',axis=1, inplace=True)
notice_df.drop('preview', axis=1, inplace=True)

engine = create_engine('mysql+pymysql://' + usr + ':' + pswd + '@127.0.0.1/' + db)
conn = engine.connect() 

#conn.execute('drop table if exists all_years_notices')
notice_df.to_sql('all_years_notices', conn, flavor=None, schema=None, if_exists='append', index=False, index_label=None, chunksize=None, dtype=None)
conn.execute('alter table all_years_notices order by notice_id asc')
