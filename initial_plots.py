
import seaborn as sb
import pandas as pd
import glob
import matplotlib.pyplot as plt



frame = pd.read_csv('', dtype={'notice_id': 'int64', 'content': 'str'})
sb.set_style('whitegrid')
plt.xlabel('notice type')
ax = sb.barplot(x='notice_type', y='notice_type', data=frame, estimator=lambda x: len(x)/len(frame) * 100, orient='v')
ax.set(ylabel='Percent')
plt.show()
