import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import argparse
from pathlib import Path
import sys

df = pd.read_csv('./data/10K_sample.tsv', sep='\t', dtype={'notice_id': 'int64', 'content': 'str'})
pd.set_option('display.max_colwidth', -1)
df.drop('Unnamed: 0',axis=1, inplace=True)
df.apply(lambda x: x.str.strip() if x.dtype == 'object' else x)


print(df.columns)
#print(df['county'].unique())

#print(df['county'].value_counts())


df = df.groupby('county').filter(lambda x: len(x) > 20)

#cnty = df[(df['county'] == 'Lafayette')]

#print(cnty['state'].unique())
#print(cnty['state'].value_counts())
print(df['county'].value_counts()[20:40])
