db = 'public_notices'
pswd = ''
usr = ''
import sqlalchemy
from sqlalchemy import create_engine
import numpy as np
import pandas as pd
from sklearn.linear_model import Ridge
from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error
from scipy.stats import pearsonr
from pprint import pprint


def addVal(d,key,v):
    try:
        d[key].append(v)
    except:
        d[key] = [v]

engine = create_engine('mysql+pymysql://' + usr + ':' + pswd + '@127.0.0.1/' + db)
conn = engine.connect() 

topic_tables = ['topics_2012', 'topics_2013', 'topics_2014']
cnty_tables = ['cnty_auction_pop_2012', 'cnty_auction_pop_2013', 'cnty_auction_pop_2014']
pop_year = ['Pop_12', 'Pop_13', 'Pop_14']

auction_dfs = []
for idx, table in enumerate(cnty_tables):
    sel = conn.execute('select * from ' + table + ' where auctions > 200 and ' + pop_year[idx] + ' > 100000')
    curr_df = pd.DataFrame(sel.fetchall())
    curr_df.columns = sel.keys()
    curr_df['auctions'] = curr_df['auctions'] / np.log(curr_df[pop_year[idx]])
    auction_dfs.append(curr_df)

print(auction_dfs[0]['auctions'].max())
# holds the topic information for each year
input_dfs = []
# iterate over all input tables and load them
for idx,table in enumerate(topic_tables):
    sel =  conn.execute("select * from " + table + " a join " + cnty_tables[idx] + " b on a.group_id = b.cnty where b.auctions > 200 and " + pop_year[idx] + ' > 100000')
    topic_df = pd.DataFrame(sel.fetchall())
    topic_df.columns = sel.keys()
    
    pprint('# unique ' + str(topic_df['group_id'].nunique()))
    group_ids = topic_df['group_id'].unique()
    final_data = []
    for i in range(topic_df['group_id'].nunique()): # each county has 2k topics
        cnty = group_ids[i] # get current group_id from all unique ones in the table
        
        cnty_data = topic_df[topic_df.group_id == cnty]['group_norm'].tolist() 
        final_data.append([cnty, cnty_data])
    
    curr_df = pd.DataFrame(final_data)
    curr_df.columns = ['cnty', 'topics']
    input_dfs.append(curr_df)

train_df = pd.DataFrame()
for idx, df in enumerate(input_dfs):
    curr_df = df.merge(auction_dfs[idx], how='left') 
    train_df = pd.concat([train_df, curr_df])

train_df.drop('notice_id', axis=1, inplace=True)


train_data = []
train_labels = []
for idx, row in train_df.iterrows():
    if len(row['topics']) < 2000:
        topics = np.append(row['topics'], np.zeros(2000 - len(row['topics'])))
    else:
        topics = row['topics']

    train_data.append(topics)
    train_labels.append(row['auctions'])
#print(train_df['auctions'].max())
x_train, x_test, y_train, y_test = train_test_split(train_data, train_labels, test_size=0.2, random_state=42)

model = Ridge(alpha=0.001)
model.fit(x_train, y_train)

preds = model.predict(x_test)

print('MSE: ' + str(mean_squared_error(y_test, preds)))
print('Pearon Corr: ' + str(pearsonr(y_test, preds))) 
