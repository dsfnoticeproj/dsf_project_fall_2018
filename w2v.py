import gensim
from gensim.test.utils import common_texts
import numpy as np
import pandas as pd
import pickle

#data_files = ['matt_label_grouped_0_100.csv']

notice_df = pd.read_csv('curr_train.csv')

def save_obj(obj, name):
    with open(name + '.pkl', 'wb+') as f:
        pickle.dump(obj, f, pickle.HIGHEST_PROTOCOL)

def load_obj(name):
    with open(name + '.pkl', 'rb') as f:
        return pickle.load(f)

public_notice_tokenized = dict()
for idx,row in notice_df.iterrows():
    k = idx
    tokens = gensim.utils.simple_preprocess(row['content'])
    public_notice_tokenized[k] = tokens


tokenized_notice_vocab = public_notice_tokenized.values()
model = gensim.models.Word2Vec(tokenized_notice_vocab,size=50, min_count=1, workers=12)

print('Training w2v...')
for k,v in public_notice_tokenized.items():
    model_name = 'notice_' + str(k)
    #model = gensim.models.Word2Vec([v],size=50, min_count=1, workers=12)
    #model.build_vocab(v) 
    model.train([v], total_examples=model.corpus_count, epochs=model.iter)
    
    all_notice_vectors = []
    #print(model.wv['hello world'])
    for word in v:
        all_notice_vectors.append(model.wv[word])
    
    #print(np.array(all_notice_vectors).shape)
    w2v_matrix = np.matrix(all_notice_vectors)
    w2v_matrix = np.mean(w2v_matrix,axis=0)
    
    #if k == 81:
    #    print(w2v_matrix)
    #    print(v)
    save_obj(w2v_matrix, './data/word2vec/' + model_name)
