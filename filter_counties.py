import pandas as pd
import numpy as np

df = pd.read_csv('./data/10K_sample.tsv', sep='\t', dtype={'notice_id': 'int64', 'content': 'str'})
pd.set_option('display.max_colwidth', -1)
df.drop('Unnamed: 0',axis=1, inplace=True)
df.apply(lambda x: x.str.strip() if x.dtype == 'object' else x)


county_list = ['Maricopa', 'Clark', 'Shelby', 'Washoe', 'Pima', 'Bernalillo', 
              'Davidson', 'Knox', 'Hinds', 'Yavapai', 'Hamilton', 'Mohave',
              'Jackson', 'Pulaski', 'Santa Fe', 'Yuma']
df = df[df['county'].isin(county_list)]

df.to_csv('./notice_data_counties.csv', sep=',')
