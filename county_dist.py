import pandas as pd
import numpy as np
import glob
import pickle
import matplotlib.pyplot as plt
from scipy.stats import chisquare
db = 'public_notices'
pswd = ''
usr = ''
import sqlalchemy
from sqlalchemy import create_engine
import collections
import seaborn as sns

engine = create_engine('mysql+pymysql://' + usr + ':' + pswd + '@127.0.0.1/' + db)
conn = engine.connect()

sel = conn.execute("select * from  mega_dist_cnty_labels_comp")
sel2 = conn.execute("select cnty from cnty_state where state = 'NJ'")

df = pd.DataFrame(sel.fetchall())
df.columns = sel.keys()

df2 = pd.DataFrame(sel2.fetchall())
df2.columns = ['cnty']


df['year'] = df.date.str[:4]
df.year=df.year.astype('int')
grouper = df.groupby(['cnty','year'])#["notice_type"] #.value_counts()

county_label_vector = collections.defaultdict(dict)
for name, group in grouper:
    if name[0] not in df2['cnty'].tolist():
        continue
    
    value_counts = group['notice_type'].value_counts()
    if len(value_counts) < 6:
        continue
    county_label_vector[name[0]][name[1]] = np.array(group['notice_type'].value_counts())

state_year_dist = collections.defaultdict(list)
keys = ['2012', '2013', '2014', '2015']
for k,v in county_label_vector.items():
    if len(v.keys()) < 4:
        continue
    
    cnty_data = [k]
    vals = list(v.values())
    for i in range(4):
        state_year_dist[keys[i]].append(vals[i])


final_dists = []
for k,v in state_year_dist.items():
    dist_mat = np.matrix(v)
    year_means = np.mean(dist_mat, axis=0)
    final_dists.append(year_means.A1)

ypos = np.arange(len(final_dists[0]))
labels = ['Auction', 'Death', 'Industrial', 'Judicial', 'Misc', 'Personal/Family']
ax = plt.bar(ypos, final_dists[0], align='center') 
plt.xticks(ypos, labels)
plt.title('NJ Label Dist 2012')
plt.xlabel('notice label')
plt.ylabel('amount')

plt.savefig('')
