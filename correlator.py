import pandas as pd
import glob
from collections import  OrderedDict
from sklearn import preprocessing
train_df=pd.read_csv("combined_labels_correlation_raw.csv")
cat_cols=["notice_type_a","notice_type"]
new_df=train_df.copy()
for col in cat_cols:
    print(col)
    lbl = preprocessing.LabelEncoder()
    lbl.fit(list(new_df[col].values.astype('str')))
    new_df[col] = lbl.transform(list(new_df[col].values.astype('str')))
dict_ann1=OrderedDict()
dict_ann2=OrderedDict()
for i in range(len(train_df)):
    dict_ann1[new_df["notice_type_a"][i]]=train_df["notice_type_a"][i]
    dict_ann2[new_df["notice_type"][i]]=train_df["notice_type"][i]
new_df = pd.get_dummies(new_df, columns=['notice_type_a','notice_type'])

cols=[ 'notice_type_a_0', 'notice_type_a_1',
       'notice_type_a_2', 'notice_type_a_3', 'notice_type_a_4',
       'notice_type_a_5', 'notice_type_a_6', 'notice_type_a_7',
       'notice_type_a_8', 'notice_type_a_9', 'notice_type_a_10',
       'notice_type_a_11', 'notice_type_a_12', 'notice_type_a_13','notice_type_0', 'notice_type_1',
       'notice_type_2', 'notice_type_3', 'notice_type_4', 'notice_type_5',
       'notice_type_6', 'notice_type_7', 'notice_type_8', 'notice_type_9',
       'notice_type_10']
col_ann1=['notice_type_a_0', 'notice_type_a_1',
       'notice_type_a_2', 'notice_type_a_3', 'notice_type_a_4',
       'notice_type_a_5', 'notice_type_a_6', 'notice_type_a_7',
       'notice_type_a_8', 'notice_type_a_9', 'notice_type_a_10',
       'notice_type_a_11', 'notice_type_a_12', 'notice_type_a_13']
col_ann2=['notice_type_0', 'notice_type_1',
       'notice_type_2', 'notice_type_3', 'notice_type_4', 'notice_type_5',
       'notice_type_6', 'notice_type_7', 'notice_type_8', 'notice_type_9',
       'notice_type_10']

dict_correlations={}
for i in range (len(col_ann1)):
    for j in range(len(col_ann2)):
        if (col_ann1[i],col_ann2[j]) not in dict_correlations.keys():
            dict_correlations[col_ann1[i],col_ann2[j]]=new_df[col_ann1[i]].corr(new_df[col_ann2[j]])

sorted_by_value = sorted(dict_correlations.items(), key=lambda kv: abs(kv[1]))
sorted_by_value=sorted_by_value[::-1] #list which hold pairwise correlation sorted by abs value
a=[]
for i in range(len(dict_ann1.keys())):
    a.append(dict_ann1[int(col_ann1[i][14:])])
for i in range(len(dict_ann2.keys())):
    a.append(dict_ann2[int(col_ann2[i][12:])])




df_corr=new_df[cols]
col_ann2=['notice_type_0', 'notice_type_1',
       'notice_type_2', 'notice_type_3', 'notice_type_4', 'notice_type_5',
       'notice_type_6', 'notice_type_7', 'notice_type_8', 'notice_type_9',
       'notice_type_10']
a=[]
for i in range(len(dict_ann2.keys())):
    a.append(dict_ann2[int(col_ann2[i][12:])])
col_ann1=['notice_type_a_0', 'notice_type_a_1',
       'notice_type_a_2', 'notice_type_a_3', 'notice_type_a_4',
       'notice_type_a_5', 'notice_type_a_6', 'notice_type_a_7',
       'notice_type_a_8', 'notice_type_a_9', 'notice_type_a_10',
       'notice_type_a_11', 'notice_type_a_12', 'notice_type_a_13']
for i in range(len(dict_ann1.keys())):
    a.append(dict_ann1[int(col_ann1[i][14:])])
print("plotting")
import seaborn as sns
sns.set(style="ticks", color_codes=True)
sns.pairplot(df_corr)
