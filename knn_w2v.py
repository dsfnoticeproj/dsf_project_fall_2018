import pandas as pd
import numpy as np
import glob
import pickle
import matplotlib.pyplot as plt
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import LogisticRegressionCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
db = 'public_notices'
pswd = ''
usr = ''
import sqlalchemy
from sqlalchemy import create_engine
import json


engine = create_engine('mysql+pymysql://' + usr + ':' + pswd + '@127.0.0.1/' + db)
conn = engine.connect() 
sel = conn.execute("select * from large_train_base where notice_id in (select notice_id from hand_labeled_train_v2)")

notice_df = pd.DataFrame(sel.fetchall())
notice_df.columns = sel.keys()

def labelEncoder(new_df,cat_cols):
    for col in cat_cols:
        lbl = preprocessing.LabelEncoder()
        lbl.fit(list(new_df[col].values.astype('str')))
        new_df[col+"_numeric"] = lbl.transform(list(new_df[col].values.astype('str')))
    
    return new_df

def fix_labels(label):
    if label == None:
        return 'Misc'
    if label == 'family/personal' or label =='Family/personal':
        return 'Personal/family'
    
    return label.capitalize()

notice_df['notice_type'] = notice_df['notice_type'].apply(fix_labels)
notice_df = labelEncoder(notice_df, ['notice_type'])
notice_df.drop('notice_type', axis=1, inplace=True)


# load in word vectors and group with appropriate level
train_vectors = []
for idx,row in notice_df.iterrows():
    word_vec = np.array(json.loads(row['word_vec']))
    label = np.array([row['notice_type_numeric']])
    
    if np.isnan(word_vec).all() or np.isnan(label).any():
        print(idx)
        print(word_vec)
        print(label)
        continue
    
    record = np.append(word_vec, label)
    train_vectors.append(record)

# Convert to appropriate train/test dataframes
train_df = pd.DataFrame(train_vectors)
labels = train_df[50] # get label data 
train_df.drop(train_df.columns[len(train_df.columns)-1], axis=1, inplace=True) # drop label from input

num_classes = labels.nunique()


##knn
n = 5
model = KNeighborsClassifier(n_neighbors=n)
model.fit(train_df, labels)

pickle.dump(model, open('./models/knn_w2v_' + str(n), 'wb'))



