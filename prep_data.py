import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import argparse
from pathlib import Path
import sys

parser = argparse.ArgumentParser()
parser.add_argument('-s', action='store', dest='start', type=int)
parser.add_argument('-e', action='store', dest='end', type=int)

args = parser.parse_args()
start = args.start
end = args.end

start_val = str(start)
end_val = str(end)
data_file = Path('./annotated_data_' + start_val + '_' + end_val + '.tsv')
if data_file.is_file():
    print('Data file for these bounds already exists. All data will be overwritten')
    print('Are you sure you want to continue?(y/n)')
    ans = input()

    if ans != 'y':
        sys.exit(0)

#df = pd.read_csv('./data/10K_sample.tsv', sep='\t', dtype={'notice_id': 'int64', 'content': 'str'})
df = pd.read_csv('./data/notice_data_counties.csv', dtype={'notice_id': 'int64', 'content': 'str'})
pd.set_option('display.max_colwidth', -1)
df.drop('Unnamed: 0',axis=1, inplace=True)
df.apply(lambda x: x.str.strip() if x.dtype == 'object' else x)

#print(df.head(2))

#print(df['content'].iloc[:2])
df['notice_type'] = 'xxxx'
matched = start
for i in range(start,end):
    print(df.iloc[i]['content'])
    print('Enter type>> ', end='', flush=True)
    notice_type = input()
    
    if notice_type =='q':
        break
    if notice_type == 's':
        continue
    
    df.iloc[i, df.columns.get_loc('notice_type')] = notice_type
    matched += 1

df[start:end].to_csv('./annotated_data_' + start_val + '_' + str(matched) + '.tsv', sep='\t')
