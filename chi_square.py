import pandas as pd
import numpy as np
import glob
import pickle
import matplotlib.pyplot as plt
from scipy.stats import chisquare
db = 'public_notices'
pswd = ''
usr = ''
import sqlalchemy
from sqlalchemy import create_engine
import collections

engine = create_engine('mysql+pymysql://' + usr + ':' + pswd + '@127.0.0.1/' + db)
conn = engine.connect()

sel = conn.execute("select * from  mega_dist_cnty_labels_comp")
sel2 = conn.execute("select cnty from cnty_state where state = 'AR'")

df = pd.DataFrame(sel.fetchall())
df.columns = sel.keys()

df2 = pd.DataFrame(sel2.fetchall())
df2.columns = ['cnty']

df['year'] = df.date.str[:4]
df.year=df.year.astype('int')
grouper = df.groupby(['cnty','year'])#["notice_type"] #.value_counts()

county_label_vector = collections.defaultdict(dict)
for name, group in grouper:
    if name[0] not in df2['cnty'].tolist():
        continue
    
    value_counts = group['notice_type'].value_counts()
    if len(value_counts) < 6:
        continue
    county_label_vector[name[0]][name[1]] = np.array(group['notice_type'].value_counts())


final_data = []
for k,v in county_label_vector.items():
    if len(v.keys()) < 4:
        continue
    cnty_data = [k]
    keys = list(v.keys())
    vals = list(v.values())
    print(k)
    for i in range(3):
        print(vals[i])
        chisq, pval = chisquare(vals[i], vals[i+1])
        cnty_data.append(chisq)
        cnty_data.append(pval)
    print('-----------')
    final_data.append(cnty_data)

cols = ['cnty', 'chi_1', 'pval_1', 'chi_2', 'pval_2', 'chi_3', 'pval_3']
table = pd.DataFrame(final_data, columns=cols)

