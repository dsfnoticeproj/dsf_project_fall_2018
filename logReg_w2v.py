import pandas as pd
import numpy as np
import glob
import pickle
import matplotlib.pyplot as plt
from sklearn.metrics import precision_recall_curve
from sklearn.metrics import precision_recall_fscore_support
from sklearn.metrics import accuracy_score
from sklearn.linear_model import LogisticRegression
from sklearn.linear_model import LogisticRegressionCV
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
db = 'public_notices'
pswd = ''
usr = ''
import sqlalchemy
from sqlalchemy import create_engine
import json


engine = create_engine('mysql+pymysql://' + usr + ':' + pswd + '@127.0.0.1/' + db)
conn = engine.connect() 
#sel = conn.execute("select * from hand_labeled_train_v2")
sel = conn.execute("select hl.notice_id, wv.word_vec, hl.notice_type from hand_labeled_train_v2 hl join all_notice_word_vectors_20epoch wv on hl.notice_id = wv.notice_id")

notice_df = pd.DataFrame(sel.fetchall())
notice_df.columns = sel.keys()

def labelEncoder(new_df,cat_cols):
    for col in cat_cols:
        lbl = preprocessing.LabelEncoder()
        lbl.fit(list(new_df[col].values.astype('str')))
        new_df[col+"_numeric"] = lbl.transform(list(new_df[col].values.astype('str')))
    
    return new_df

def fix_labels(label):
    if label == None:
        return 'Misc'
    if label == 'family/personal' or label =='Family/personal':
        return 'Personal/family'
    
    return label.capitalize()

notice_df['notice_type'] = notice_df['notice_type'].apply(fix_labels)
notice_df = labelEncoder(notice_df, ['notice_type'])
notice_df.drop('notice_type', axis=1, inplace=True)


# load in word vectors and group with appropriate level
train_vectors = []
for idx,row in notice_df.iterrows():
    word_vec = np.array(json.loads(row['word_vec']))
    label = np.array([row['notice_type_numeric']])
    
    if np.isnan(word_vec).all() or np.isnan(label).any():
        print(idx)
        print(word_vec)
        print(label)
        continue
    
    record = np.append(word_vec, label)
    train_vectors.append(record)

# Convert to appropriate train/test dataframes
train_df = pd.DataFrame(train_vectors)
labels = train_df[50] # get label data 
train_df.drop(train_df.columns[len(train_df.columns)-1], axis=1, inplace=True) # drop label from input

train_data, test_data, train_labels, test_labels = train_test_split(train_df, labels, random_state=42)
num_classes = labels.nunique()


##LR
model = LogisticRegressionCV(cv=5, random_state=0, solver='liblinear',multi_class='ovr', n_jobs=-1).fit(train_data, train_labels)
ypred = model.predict(test_data)
accuracy = accuracy_score(test_labels,ypred)
precision, recall, fscore, support = precision_recall_fscore_support(test_labels, ypred, average=None)


print('accuracy: {}'.format(accuracy))
print('mean precision: {}'.format(np.mean(precision)))
print('mean recall: {}'.format(np.mean(recall)))
print('mean fscore: {}'.format(np.mean(fscore)))
#print('support: {}'.format(support))

pickle.dump(model, open('./models/logReg_w2v', 'wb'))
